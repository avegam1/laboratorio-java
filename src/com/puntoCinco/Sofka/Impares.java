package com.puntoCinco.Sofka;

import javax.swing.*;

public class Impares {
    public void calcularPares(){
        int x = 1;
        while (x <= 100) {
            int temp = x % 2;
            if (temp == 0) {
                JOptionPane.showMessageDialog(null,"El número " + x + " es par.");
            }
            x++;
        }
    }
    public void calcularImpares(){
        int y = 1;
        while (y <= 100) {
            int temp = y % 2;
            if (temp != 0) {
                JOptionPane.showMessageDialog(null,"El número " + y + " es impar.");
            }
            y++;
        }
    }
}
