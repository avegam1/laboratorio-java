package com.puntoTrece.Sofka;

public class Hora {
    public static void fecha(){
        date dateNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd '-' HH:mm:ss");

        System.out.println("Fecha actual: " + ft.format(dateNow));
    }

}
